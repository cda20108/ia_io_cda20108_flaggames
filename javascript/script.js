
let jsonVariable = {
  counter : 0,
  intervalId : null,
  minute : 0,
  compter : 0,
  minuteTotal : 0,
  secondeTotal : 0,
  clickTotal : 0,
  score : 0,
  scoreTotal : 0,
  manche : 1,
  colorL : "rgb(46, 106, 229)",
  colorC : "rgb(255, 255, 255)",
  colorR : "rgb(212, 6, 4)",
  color : ["rgb(46, 106, 229)", "rgb(255, 255, 255)", "rgb(212, 6, 4)"],
  colorJoker : " ",
  pays : ["France", "Belgique", "Allemagne", "Hollande", "Pologne", "Rep. Tchèque"],
  paysActuel : 0
}

let objLinea = JSON.stringify(jsonVariable);
localStorage.setItem("obj",objLinea);


function getRandomInt(max) {
  return Math.floor(Math.random() * max);
}

function bip() {

  let objLinea = localStorage.getItem("obj");
  let objJson = JSON.parse(objLinea);

  objJson.counter += 1;
  objJson.secondeTotal += 1;
  if(objJson.counter >= 60) {
    objJson.counter = 0;
    objJson.minute += 1;
  }
  else if (objJson.minute == 2) {
      finish();
  }

  else {
      $("#bip").text(objJson.minute + " minute(s) " + objJson.counter + " seconde(s)");
  }

  localStorage.setItem("obj",JSON.stringify(objJson));

}




function compterClick(){

  let objLinea = localStorage.getItem("obj");
  let objJson = JSON.parse(objLinea);

  objJson.compter ++;
  $("#compter").text(objJson.compter + " click(s) ");

  localStorage.setItem("obj",JSON.stringify(objJson));

}





function changeColor(element) {

  let objLinea = localStorage.getItem("obj");
  let objJson = JSON.parse(objLinea);

  element.css("background-color", objJson.color[objJson.colorIncrement]);
  objJson.colorIncrement ++;
  if ($(".pays").text() == objJson.pays[4]) {
    if (objJson.colorIncrement > 1) {
      objJson.colorIncrement = -1;
    }
  }else {
    if (objJson.colorIncrement > 2) {
      objJson.colorIncrement = 0;
    }
  }

  localStorage.setItem("obj",JSON.stringify(objJson));

  // verification de win à chaque click
  setTimeout(function () {
    checkWin();
  }, 300);

}





function changementDrapeau() {

  let objLinea = localStorage.getItem("obj");
  let objJson = JSON.parse(objLinea);

  objJson.paysActuel++;

  if (objJson.paysActuel > 5) {
    objJson.paysActuel = 0;
  }


  $("#droite").css("display" , "inline");

  localStorage.setItem("obj",JSON.stringify(objJson));

  objLinea = localStorage.getItem("obj");
  objJson = JSON.parse(objLinea);

  $(".pays").text(objJson.pays[objJson.paysActuel]);
  $(".gauche").css("background-color", "white");
  $(".centre").css("background-color", "white");
  $(".droite").css("background-color", "white");

  if (objJson.paysActuel == 0) {
    // France

    objJson.colorL = "rgb(46, 106, 229)";
    objJson.colorC = "rgb(255, 255, 255)";
    objJson.colorR = "rgb(212, 6, 4)";
    objJson.color = [objJson.colorR, objJson.colorL, objJson.colorC];


    $(".drapeau").css({
      "flex-direction" : " ",
      "margin" : 'auto',
      "margin-top" : '20px'});

    $(".gauche").css({
          height : '300px',
          width : '125px',});
    $(".centre").css({
          height : '300px',
          width : '125px',
          "-webkit-clip-path" : "none" ,
          "clip-path" : "none",
          position : "static"});
    $(".droite").css({
          height : '300px',
          width : '125px'});

    $(".drapeau").css({
        "flex-direction" : 'row'});

  }else if (objJson.paysActuel == 1) {
    // Belgique

    objJson.colorL = "rgb(20, 20, 20)";
    objJson.colorC = "rgb(250, 242, 42)";
    objJson.colorR = "rgb(212, 6, 4)";
    objJson.color = [objJson.colorC, objJson.colorR, objJson.colorL];

  }else if (objJson.paysActuel == 2) {
    // Allemagne

    $(".gauche").css({
          height : '100px',
          width : '375px'});
    $(".centre").css({
          height : '100px',
          width : '375px'});
    $(".droite").css({
          height : '100px',
          width : '375px'});

    $(".drapeau").css({
        "flex-direction" : 'column',
        "margin-left" : '57px'});

    objJson.colorL = "rgb(20, 20, 20)";
    objJson.colorC = "rgb(212, 6, 4)";
    objJson.colorR = "rgb(250, 242, 42)";
    objJson.color = [objJson.colorR, objJson.colorL, objJson.colorC];

  }else if (objJson.paysActuel == 3) {
    // Hollande

    objJson.colorL = "rgb(212, 6, 4)";
    objJson.colorC = "rgb(255, 255, 255)";
    objJson.colorR = "rgb(46, 106, 229)";
    objJson.color = [objJson.colorR, objJson.colorL, objJson.colorC];

  }else if (objJson.paysActuel == 4) {
    // Pollogne

    $(".gauche").css({
          height : '150px',
          width : '375px'});

    $(".centre").css("display", "none");

    $(".droite").css({
          height : '150px',
          width : '375px'});

    objJson.colorL = "rgb(255, 255, 255)";
    objJson.colorR = "rgb(212, 6, 4)";
    objJson.color = [objJson.colorR, objJson.colorL];

  }else if (objJson.paysActuel == 5) {
    // Rep. Tchèque

    $(".gauche").css({
          height : '150px',
          width : '375px'});

    $(".centre").css({
          display : 'flex',
          position : 'absolute',
          height: '300px',
          width: '160px',
          "-webkit-clip-path": "polygon(0 0, 0 100%, 100% 50%)",
          "clip-path" : "polygon(0 0, 0 100%, 100% 50%)",
          border: 'solid 3px rgb(19, 19, 19)'
        });

    $(".droite").css({
          height : '150px',
          width : '375px'});

    objJson.colorL = "rgb(255, 255, 255)";
    objJson.colorC = "rgb(46, 106, 229)";
    objJson.colorR = "rgb(212, 6, 4)";
    objJson.color = [objJson.colorR, objJson.colorL, objJson.colorC];

  }
  objJson.manche ++;

  if (objJson.manche >= 7){
    finish();
  }

  localStorage.setItem("obj",JSON.stringify(objJson));
}

function score(){
  let objLinea = localStorage.getItem("obj");
  let objJson = JSON.parse(objLinea);

  objJson.score = 0;

  if(objJson.counter <= 3 && objJson.compter <= 5){
    objJson.score = objJson.score + 3;
    objJson.scoreTotal = objJson.scoreTotal + objJson.score;
  }
  else if (objJson.counter > 3 && objJson.compter > 5 && objJson.compter <= 7){
    objJson.score = objJson.score + 2;
    objJson.scoreTotal = objJson.scoreTotal + objJson.score;
  }
  else{
    objJson.score = objJson.score + 1;
    objJson.scoreTotal = objJson.scoreTotal + objJson.score;
  }

  localStorage.setItem("obj",JSON.stringify(objJson));
}

function finish() {

  let objLinea = localStorage.getItem("obj");
  let objJson = JSON.parse(objLinea);

  plateauFin();
  clearInterval(objJson.intervalId);
  objJson.compter = 0;
  objJson.counter = -1;
  objJson.minute = 0;
  objJson.manche = 1;
  objJson.intervalId = null;
  localStorage.setItem("obj",JSON.stringify(objJson));
}

function popUpScore(){

  let popUpScore = $(".popUpScore").css("display", "block");

  score();

  let objLinea = localStorage.getItem("obj");
  let objJson = JSON.parse(objLinea);

  $("#clickPartie").text("Vous avez fait " + objJson.compter + " clicks.");
  $("#compterPartie").text("Votre partie a durée au total " + objJson.minute + "minute(s) et " + objJson.counter + " seconde(s).");
  $("#scorePartie").text("Vous avez fait " + objJson.score + " points.");

  localStorage.setItem("obj",JSON.stringify(objJson));
}

function continuer(){

  let popUpScore = $(".popUpScore").css("display", "none");
  $(".drapeau").css("display", "flex");
  $("#abandon").css("display", "inline");
  $("#joker").css("display", "inline");

  let objLinea = localStorage.getItem("obj");
  let objJson = JSON.parse(objLinea);

  $("#manche").text("Manche : " + objJson.manche + "/6");
  objJson.clickTotal = objJson.compter + objJson.clickTotal;
  objJson.counter = -1;
  objJson.minute = 0;
  objJson.compter = 0;


  localStorage.setItem("obj",JSON.stringify(objJson));
}

function plateauFin(){

  let objLinea = localStorage.getItem("obj");
  let objJson = JSON.parse(objLinea);

  let cacherJeu = $(".plateau").css("display", "none");
  $(".plateauFin").css("display", "block");

  $("#clickFin").text("Vous avez fait " + objJson.clickTotal + " clicks.");
  $("#scoreFinPartie").text("Vous avez fait " + objJson.scoreTotal + " points.");
  if (objJson.minute != 2){

   while (objJson.secondeTotal >= 60){
      objJson.secondeTotal = objJson.secondeTotal - 60;
      objJson.minuteTotal ++;
    }
    $("#compterFin").text("Votre partie a durée au total " + objJson.minuteTotal + " minute(s) et " + objJson.secondeTotal + " seconde(s).");
  }else{
    $("#compterFin").text("Le temps maximum est écoulé ! Vous ferez mieux la prochaine fois !");
    $("#mancheFin").text("Vous avez perdu à la manche : " + objJson.manche + "/6.");
  }
  localStorage.setItem("obj",JSON.stringify(objJson));
}

function checkWin() {

  let objLinea = localStorage.getItem("obj");
  let objJson = JSON.parse(objLinea);

  let gauche = $(".gauche").css("background-color");
  let centre = $(".centre").css("background-color");
  let droite = $(".droite").css("background-color");

  if ($(".pays").text() == objJson.pays[4]) {
    if ((gauche == objJson.colorL) && (droite == objJson.colorR)) {
      $(".drapeau").css("display", "none");
      $("#abandon").css("display", "none");
      $("#joker").css("display", "none");
      popUpScore();
      changementDrapeau();

      return true;
    }
  }else {
    if ((gauche == objJson.colorL) && (centre = objJson.colorC) && (droite == objJson.colorR)) {
      $(".drapeau").css("display", "none");
      $("#abandon").css("display", "none");
      $("#joker").css("display", "none");
      popUpScore();
      changementDrapeau();

      return true;

    }
  }
}

function abandonner(){

    let objLinea = localStorage.getItem("obj");
    let objJson = JSON.parse(objLinea);

    localStorage.setItem("obj",JSON.stringify(objJson));
    objJson.manche++;
    $("#manche").text("Manche : " + objJson.manche + "/6");

    changementDrapeau();

    $("#joker").css("display" , "inline");

}

function cacher(){
  let cacher = $(".plateauDepart").css("display", "none");
}



function joker() {

  let objLinea = localStorage.getItem("obj");
  let objJson = JSON.parse(objLinea);

  let randomColor = objJson.color[getRandomInt(2)];

  if (randomColor == objJson.colorL) {
    $(".gauche").css("background-color" , randomColor);
  }else if (randomColor == objJson.colorC) {
    $(".centre").css("background-color" , randomColor);
  }else if (randomColor == objJson.colorR) {
    $(".droite").css("background-color" , randomColor);
  }
    $("#joker").css("display" , "none");
}


function allClicks() {


  let objLinea = localStorage.getItem("obj");
  let objJson = JSON.parse(objLinea);

  $(".gauche").click(function() {
    let gauche = $(".gauche");
    changeColor(gauche);
  });

  $(".centre").click(function() {
    let centre = $(".centre");
    changeColor(centre);
  });

  $(".droite").click(function() {
    let droite = $(".droite");
    changeColor(droite);
  });

  $(".drapeau").click(function() {
    compterClick();
  });

  $("#abandon").click(function() {
    abandonner();
  });

  $("#joker").click(function() {
    joker();
  });

  //Problème au niveau des clicks (essaies)
  $("#rejouer").click(function(){

    $(".drapeau").css("display", "flex");
    $("#abandon").css("display", "inline");
    $("#joker").css("display", "inline");



    objJson.counter = 0;
    objJson.minute = 0;
    objJson.compter = 0;
    objJson.minuteTotal = 0;
    objJson.secondeTotal = 0;
    objJson.clickTotal = 0;
    objJson.manche = 1;
    objJson.paysActuel = 0;

    $("#manche").text("Manche : " + objJson.manche + "/6");

    objJson.intervalId = setInterval(bip, 1000);

    localStorage.setItem("obj",JSON.stringify(objJson));

    $(".plateauFin").css("display", "none");
    $(".popUpScore").css("display", "none");
    $(".plateau").css("display", "block");

  });

}






function start(){

  let objLinea = localStorage.getItem("obj");
  let objJson = JSON.parse(objLinea);

  let popUpScore = $(".popUpScore").css("display", "none");

  $("#bip").text(objJson.minute + " minute(s) " + objJson.counter + " seconde(s)");

  let jeu = $(".plateau").css("display", "block");

  cacher();

  $("#manche").text("Manche : " + objJson.manche + "/6");
  $(".pays").text(objJson.pays[objJson.paysActuel]);

  objJson.intervalId = setInterval(bip, 1000);
  allClicks();

  localStorage.setItem("obj",JSON.stringify(objJson));

}
